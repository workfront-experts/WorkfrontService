<?php namespace Simplus\WorkfrontService\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class InstantiateWorkfrontClient {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->query('sessionID') || $request->header('SessionID') || $request->get('sessionID') || \Session::has('sessionID')) {
            $this->setSessionID($request);
        } else if($request->query('apiKey') || $request->header('ApiKey') || $request->get('apiKey') || \Session::has('apiKey')) {
            $this->setApiKey($request);
        } else {
            $this->loginWorkfront($request);
        }

        $this->setInstanceType($request);

        return $next($request);
    }

    private function setSessionID(Request $request)
    {
        $sessionId = $this->getSessionID($request);

        if ($sessionId === false) {
            // TODO: If there is no session ID redirect to error
            abort(401,'You have not provided a Workfront SessionID');
        }

        session(['sessionID' => $sessionId]);

        // If there is a session ID, make query to get current signed user
        $workfront = app('workfront')->setSessionID($sessionId);

//        if(strpos($request->server('HTTP_REFERER'), \App::make('url')->to('/')) === false) {
//            $referer = parse_url($request->server('HTTP_REFERER'));
//
//            $workfront->setBaseUrl($referer['scheme'] . '//' . $referer['host']);
//        } else {
//            $this->setInstanceType($request);
//        }
    }

    private function getSessionID($request)
    {
        // Check if there is a session ID in a header
        if($request->header('SessionID') !== null) {
            return $request->header('SessionID');
        } else if($request->get('sessionID') !== null) {
            return $request->get('sessionID');
            // Check if there is a session ID in the query params
        } else if($request->query('sessionID') !== null) {
            return $request->query('sessionID');
        } else if(\Session::has('sessionID')) {
            return session('sessionID');
        } else {
            return false;
        }
    }

    private function getApiKey(Request $request)
    {
        // Check if there is a session ID in a header
        if($request->header('ApiKey') !== null) {
            return $request->header('ApiKey');
        } else if($request->get('apiKey') !== null) {
            return $request->get('apiKey');
            // Check if there is a session ID in the query params
        } else if($request->query('apiKey') !== null) {
            return $request->query('apiKey');
        } else if(\Session::has('apiKey')) {
            return session('apiKey');
        } else {
            return false;
        }
    }

    private function loginWorkfront(Request $request)
    {
        $login = config('workfront.login');
        if(isset($login)) {

            if(isset($login['url'])) {
                $workfront = app('workfront')->setBaseUrl($login['url']);
            } else {
                $workfront = app('workfront')->setBaseUrl($this->getBaseUrl($request));
            }

            if(isset($login['username']) && isset($login['apiKey'])) {
                $workfront->setUsername($login['username']);
                $workfront->setApiKey($login['apiKey']);
            } else if(isset($login['username']) && isset($login['password'])) {
                $workfront->login($login['username'],$login['password']);
            }
        }
    }

    private function getBaseUrl(Request $request)
    {
        if(strpos($request->server('HTTP_REFERER'), \App::make('url')->to('/')) === false) {
            $referer = parse_url($request->server('HTTP_REFERER'));

            return $referer['scheme'] . '//' . $referer['host'];
        } else {
            $instanceType = $request->get('instanceType');

            if($instanceType !== null) {
                session(['instanceType' => $instanceType]);
            } else if(\Session::has('instanceType')) {
                $instanceType = session('instanceType');
            } else {
                $instanceType = 'production';
            }

            $domain = app('workfront')->getDomain($instanceType);

            if($request->route()->parameter('instance')) {
                return 'https://' . $request->route()->parameter('instance') . '.' . $domain;
            } else {
                return false;
            }
        }
    }

    private function setApiKey($request)
    {
        $key = $this->getApiKey($request);

        if ($key === false) {
            // TODO: If there is no session ID redirect to error
            abort(401,'You have not provided a Workfront Api Key');
        }

        session(['apiKey' => $key]);

        $workfront = app('workfront')->setApiKey($key);
    }

    /**
     * @param Request $request
     * @param $workfront
     */
    private function setInstanceType(Request $request)
    {
        $instanceType = $request->get('instanceType');

        $workfront = app('workfront');

        if ($instanceType !== null) {
            session(['instanceType' => $instanceType]);
            $workfront->setInstanceType($instanceType);
        } else if (\Session::has('instanceType')) {
            $workfront->setInstanceType(session('instanceType'));
        } else {
            $workfront->setInstanceType('production');
        }
    }
}