<?php namespace Simplus\WorkfrontService\Middleware;

use Closure;

class RedirectIfNoValidSessionID
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $workfront = app('workfront');

        if($workfront->isValidSession() === false) {
            abort(401,'The provided Workfront SessionID is not valid.');
        }

        return $next($request);
    }

}
