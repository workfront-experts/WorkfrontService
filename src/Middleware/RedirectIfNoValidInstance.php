<?php namespace Simplus\WorkfrontService\Middleware;

use Closure;

class RedirectIfNoValidInstance
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route = $request->route();

        // If there is a session ID, make query to get current signed user
        $workfront = app('workfront');

        foreach(config('workfront.apps') as $app) {
            if(in_array($route->parameter('instance'),config($app . '.validInstances'))) {
                $workfront->setInstance($route->parameter('instance'));
                $workfront->validateSession();
            }
        }

        return $next($request);
    }
}