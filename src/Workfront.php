<?php namespace Simplus\WorkfrontService;

use Outbox\Client\Workfront\Impersonate;
use Outbox\Client\Workfront\WorkfrontBatch;
use Outbox\Client\Workfront\WorkfrontClient;

class Workfront
{
    private $instance;
    /**
     * @var WorkfrontClient
     */
    private $client;
    private $domains = [
        'production'  => 'my.workfront.com',
        'sandbox'     => 'preview.workfront.com',
        'refreshable' => 'workfront.com'
    ];
    private $instanceType = 'production';
    private $validSession = false;
    /** @var  Impersonate */
    private $impersonateClient;
    /** @var  WorkfrontBatch */
    private $batchClient;

    public function __construct()
    {
        $this->client = new WorkfrontClient();
    }

    public function getCurrentUser()
    {
        return $this->client->search('user', ['id' => '$$USER'], ['*'])[0];
    }

    public function setInstanceType($instanceType)
    {
        if (in_array($instanceType, array_keys($this->domains))) {
            $this->instanceType = $instanceType;
        } else {
            throw new \Exception('The instance type ' . $instanceType . ' is not an approved type.');
        }

        return $this;
    }

    public function setInstance($instance)
    {
        $this->instance = $instance;
        $this->client->setBaseURL($this->getInstanceUrl());
        return $this;
    }

    public function setSessionID($sessionID)
    {
        $this->client->setSessionID($sessionID);
        if (isset($this->instance)) {
            $this->validateSession();
        }
        return $this;
    }

    public function setApiKey($key)
    {
        $this->client->setApiKey($key);
        if (isset($this->instance)) {
            $this->validateSession();
        }
        return $this;
    }

    public function getInstanceUrl()
    {
        return $this->instance . '.' . $this->getDomain($this->instanceType);
    }

    public function getDomain($type)
    {
        return $this->domains[$type];
    }

    public function validateSession()
    {
        try {
            $response = $this->client->search('user', ['ID' => '$$USER_ID']);

            $this->validSession = true;
        } catch (\Exception $e) {
            $this->validSession = false;
        }

        return $this;
    }

    public function isValidSession()
    {
        return $this->validSession;
    }

    public function impersonate()
    {
        $this->instantiateImpersonationClass();

        return $this;
    }

    public function addLoginToImpersonate($email, array $login)
    {
        $this->instantiateImpersonationClass();

        $this->impersonateClient->addLogin($email, $login);
    }

    public function refreshImpersonateLogins()
    {
        $this->impersonateClient->refreshLogins();
    }

    public function resetImpersonateLogins()
    {
        $this->impersonateClient->resetLogins();
    }

    public function asUser($email)
    {
        return $this->impersonateClient->runAsUser($email);
    }

    public function batch()
    {
        $this->instantiateBatchClass();

        return $this->batchClient;
    }

    public function __call($name, array $arguments)
    {
        if (in_array($name, get_class_methods('Outbox\Client\Workfront\WorkfrontClient'))) {
            return call_user_func_array([$this->client, $name], $arguments);
        }
    }

    protected function instantiateImpersonationClass()
    {
        if (!isset($this->impersonateClient)) {
            $this->impersonateClient = new Impersonate();
            $this->impersonateClient->setClient($this->client);
        }
    }

    protected function instantiateBatchClass()
    {
        if (!isset($this->batchClient)) {
            $this->instantiateImpersonationClass();
            $this->batchClient = new WorkfrontBatch($this->client,$this->impersonateClient);
        }
    }
}