<?php namespace Simplus\WorkfrontService;

use Illuminate\Support\ServiceProvider;

class WorkfrontServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('workfront', function() {
            return new Workfront();
        });
    }

}
